-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        wil
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-22 20:54
-- Created:       2022-02-22 20:54
PRAGMA foreign_keys = OFF;

-- Schema: mydb
-- ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "Users"(
  "idUsers" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "username" VARCHAR(45) NOT NULL,
  "password" VARCHAR(45) NOT NULL,
  "email" VARCHAR(45) NOT NULL,
  "created_date" DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
);
COMMIT;
