import { Application } from "express";
import HomeController from "./controllers/HomeController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/users', (req, res) =>
    {
        HomeController.index(req, res);
    });

}
